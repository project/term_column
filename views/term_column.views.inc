<?php

/**
 * Implements hook_views_data_alter().
 */
function term_column_views_data_alter(&$data) {
  $data['node']['term_node_tid_column'] = array(
    'title' => t('Taxonomy term on a node'),
    'field' => array(
      'title' => t('Taxonomy term'),
      'help' => t('Display a specific taxonomy term associated with a node from specified vocabulary.'),
      'handler' => 'term_column_views_handler_field_term_node_tid_column',
      'no group by' => TRUE,
    ),
  );
}
