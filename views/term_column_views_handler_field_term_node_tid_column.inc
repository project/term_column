<?php

/**
 * Field handler to display all taxonomy terms of a node.
 *
 * @ingroup views_field_handlers
 */
class term_column_views_handler_field_term_node_tid_column extends views_handler_field_prerender_list {
  function init(&$view, &$options) {
    parent::init($view, $options);
    // @todo: Wouldn't it be possible to use $this->base_table and no if here?
    if ($view->base_table == 'node_revision') {
      $this->additional_fields['nid'] = array('table' => 'node_revision', 'field' => 'nid');
    }
    else {
      $this->additional_fields['nid'] = array('table' => 'node', 'field' => 'nid');
    }
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['link_to_taxonomy'] = array('default' => TRUE);
    $options['vocabulary'] = array('default' => array());
    $options['term'] = array('default' => array());

    return $options;
  }

  /**
   * Provide "link to term" option.
   */
  function options_form(&$form, &$form_state) {
    $vocabs = array();
    $vocabularies = taxonomy_get_vocabularies();
    foreach ($vocabularies as $voc) {
      $vocabs[$voc->vid] = check_plain($voc->name);
    }

    $form['vocabulary'] = array(
      '#prefix' => '<div><div id="edit-options-vocabulary">',
      '#suffix' => '</div></div>',
      '#type' => 'select',
      '#title' => t('Vocabulary'),
      '#options' => $vocabs,
      '#default_value' => $this->options['vocabulary'],
      '#ajax' => array(
        'callback' => array('term_column_views_handler_field_term_node_tid_column', ),
      ),
    );

    if (isset($form_state['values']['vocabulary'])) {
      $vocab = $form_state['values']['vocabulary'];
    }
    elseif (isset($this->options['vocabulary'])) {
      $vocab = $this->options['vocabulary'];
    }
    else {
      $vocab = NULL;
    }

    $term_options = array();
    if ($vocab) {
      if ($terms = taxonomy_get_tree($vocab)) {
        foreach ($terms as $term) {
          $term_options[$term->tid] = str_repeat('-', $term->depth) . $term->name;
        }
      }
    }

    $form['term'] = array(
      '#prefix' => '<div><div id="edit-options-term">',
      '#suffix' => '</div></div>',
      '#type' => 'select',
      '#title' => t('Term'),
      '#options' => $term_options,
      '#default_value' => $this->options['term'],
    );

    $form['link_to_taxonomy'] = array(
      '#title' => t('Link this field to its term page'),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['link_to_taxonomy']),
    );


    parent::options_form($form, $form_state);
  }

  public static function ajax($form, $form_state) {
    return $form['term'];
  }

  /**
   * Add this term to the query
   */
  function query() {
    $this->add_additional_fields();
  }

  function pre_render(&$values) {
    $this->field_alias = $this->aliases['nid'];
    $nids = array();
    foreach ($values as $result) {
      if (!empty($result->{$this->aliases['nid']})) {
        $nids[] = $result->{$this->aliases['nid']};
      }
    }

    if ($nids && $this->options['term']) {
      $query = db_select('taxonomy_index', 'tn');
      $query->innerJoin('taxonomy_term_data', 'td', 'tn.tid = td.tid');
      $query->fields('tn');
      $query->fields('td', array('name'));
      $query->condition('tn.nid', $nids);
      $query->condition('tn.tid', $this->options['term']);
      $result = $query->execute();

      foreach ($result as $term) {
        $this->items[$term->nid][$term->tid]['name'] = check_plain($term->name);
        $this->items[$term->nid][$term->tid]['tid'] = $term->tid;

        if (!empty($this->options['link_to_taxonomy'])) {
          $this->items[$term->node_nid][$term->tid]['make_link'] = TRUE;
          $this->items[$term->node_nid][$term->tid]['path'] = 'taxonomy/term/' . $term->tid;
        }
      }
    }
  }

  function render_item($count, $item) {
    return $item['name'];
  }

  function document_self_tokens(&$tokens) {
    $tokens['[' . $this->options['id'] . '-tid' . ']'] = t('The taxonomy term ID for the term.');
    $tokens['[' . $this->options['id'] . '-name' . ']'] = t('The taxonomy term name for the term.');
  }

  function add_self_tokens(&$tokens, $item) {
    foreach(array('tid', 'name', 'vocabulary_machine_name', 'vocabulary') as $token) {
      // Replace _ with - for the vocabulary machine name.
      $tokens['[' . $this->options['id'] . '-' . str_replace('_', '-', $token). ']'] = isset($item[$token]) ? $item[$token] : '';
    }
  }
}
